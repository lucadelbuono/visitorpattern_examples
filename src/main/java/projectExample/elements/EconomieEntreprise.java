package VisitorPattern.projectExample.elements;

import VisitorPattern.projectExample.visitors.HegVisitor;

import java.util.ArrayList;
import java.util.List;

public class EconomieEntreprise extends Heg{

    protected List<String> options;

    public EconomieEntreprise(int difficulte, String exigence, List<String> options) {
        super(difficulte, exigence);
        this.options = options;
    }

    @Override
    public String accepter(HegVisitor visitor) {
        return visitor.visiterFee(this);
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }
}
