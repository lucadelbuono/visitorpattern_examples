package VisitorPattern.projectExample.elements;

import VisitorPattern.projectExample.visitors.HegVisitor;

public abstract class Heg {
    public abstract String accepter(HegVisitor visitor);
    protected int difficulte;
    protected String exigence;

    public Heg(int difficulte, String exigence) {
        this.difficulte = difficulte;
        this.exigence = exigence;
    }

    public int getDifficulte() {
        return difficulte;
    }

    public void setDifficulte(int difficulte) {
        this.difficulte = difficulte;
    }

    public String getExigence() {
        return exigence;
    }

    public void setExigence(String exigence) {
        this.exigence = exigence;
    }
}
