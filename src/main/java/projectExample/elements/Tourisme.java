package VisitorPattern.projectExample.elements;

import VisitorPattern.projectExample.visitors.HegVisitor;

import java.util.ArrayList;
import java.util.List;

public class Tourisme extends Heg{

    protected List<String> langues;

    public Tourisme(int difficulte, String exigence, List<String> langues) {
        super(difficulte, exigence);
        this.langues = langues;
    }

    @Override
    public String accepter(HegVisitor visitor) {
        return visitor.visiterFto(this);
    }

    public List<String> getLangues() {
        return langues;
    }

    public void setLangues(ArrayList<String> langues) {
        this.langues = langues;
    }
}
