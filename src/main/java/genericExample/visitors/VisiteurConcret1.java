package VisitorPattern.genericExample.visitors;

import VisitorPattern.genericExample.elements.ElementConcretA;
import VisitorPattern.genericExample.elements.ElementConcretB;

public class VisiteurConcret1 implements Visiteur{
    @Override
    public void visiterElementConcretA(ElementConcretA elementConcretA) {
        System.out.println("Je suis le visiteur concret 1 et je visite l'élément concret A");
    }

    @Override
    public void visiterElementConcretB(ElementConcretB elementConcretB) {
        System.out.println("Je suis le visiteur concret 1 et je visite l'élément concret B");
    }
}
