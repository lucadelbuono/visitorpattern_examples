package VisitorPattern.genericExample.elements;

import VisitorPattern.genericExample.visitors.Visiteur;

public class ElementConcretB implements Element{
    @Override
    public void accepter(Visiteur v) {
        v.visiterElementConcretB(this);
    }
}
