package VisitorPattern.projectExample;

import VisitorPattern.projectExample.elements.EconomieEntreprise;
import VisitorPattern.projectExample.elements.Heg;
import VisitorPattern.projectExample.elements.InformatiqueGestion;
import VisitorPattern.projectExample.elements.Tourisme;
import VisitorPattern.projectExample.visitors.Employeur;
import VisitorPattern.projectExample.visitors.FuturEtudiant;
import VisitorPattern.projectExample.visitors.HegVisitor;
import VisitorPattern.projectExample.visitors.Journaliste;

import java.util.Arrays;

public class Demo {

    public static void main(String[] args) {
        HegVisitor employeur = new Employeur();
        HegVisitor futurEtudiant = new FuturEtudiant();
        HegVisitor journaliste = new Journaliste();

        InformatiqueGestion fig = new InformatiqueGestion(10,"être bon en maths", Arrays.asList("Java", "C#", "SQL"));
        EconomieEntreprise fee = new EconomieEntreprise(7, "être bon en comptabilité", Arrays.asList("Fiscalité", "Droit international", "Comptabilité avancée"));
        Tourisme fto = new Tourisme(5, "avoir un bon relationnel", Arrays.asList("Anglais", "Allemand", "Espagnol", "Italien"));

        Heg[] team_heg = new Heg[]{fig, fee, fto};

        System.out.println("Employeur");
        doOperation(team_heg,employeur);

        System.out.println("Futur étudiant");
        doOperation(team_heg,futurEtudiant);

        System.out.println("Journaliste");
        doOperation(team_heg,journaliste);

    }

    private static void doOperation(Heg[] team_heg, HegVisitor visitor){
        for (Heg heg : team_heg){
            System.out.println(heg.accepter(visitor));
        }
    }
}
