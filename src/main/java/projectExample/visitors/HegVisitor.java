package VisitorPattern.projectExample.visitors;

import VisitorPattern.projectExample.elements.EconomieEntreprise;
import VisitorPattern.projectExample.elements.InformatiqueGestion;
import VisitorPattern.projectExample.elements.Tourisme;

public interface HegVisitor {
    String visiterFig(InformatiqueGestion fig);
    String visiterFee(EconomieEntreprise fee);
    String visiterFto(Tourisme fto);
}
