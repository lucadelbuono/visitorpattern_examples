package VisitorPattern.genericExample.elements;

import VisitorPattern.genericExample.visitors.Visiteur;

public interface Element {
    void accepter(Visiteur v);
}