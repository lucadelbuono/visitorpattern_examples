package VisitorPattern.genericExample.elements;

import VisitorPattern.genericExample.visitors.Visiteur;

public class ElementConcretA implements Element{
    @Override
    public void accepter(Visiteur v) {
        v.visiterElementConcretA(this);
    }
}
