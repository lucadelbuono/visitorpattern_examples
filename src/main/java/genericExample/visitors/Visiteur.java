package VisitorPattern.genericExample.visitors;

import VisitorPattern.genericExample.elements.ElementConcretA;
import VisitorPattern.genericExample.elements.ElementConcretB;

public interface Visiteur {
    void visiterElementConcretA(ElementConcretA elementConcretA);
    void visiterElementConcretB(ElementConcretB elementConcretB);
}
