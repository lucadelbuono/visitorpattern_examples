package VisitorPattern.genericExample;

import VisitorPattern.genericExample.elements.Element;
import VisitorPattern.genericExample.elements.ElementConcretA;
import VisitorPattern.genericExample.elements.ElementConcretB;
import VisitorPattern.genericExample.visitors.Visiteur;
import VisitorPattern.genericExample.visitors.VisiteurConcret1;
import VisitorPattern.genericExample.visitors.VisiteurConcret2;

public class Client {
    public static void main(String[] args) {
        Visiteur visiteurConcret1 = new VisiteurConcret1();
        Visiteur visiteurConcret2 = new VisiteurConcret2();

        Element elementConcretA = new ElementConcretA();
        Element elementConcretB = new ElementConcretB();

        elementConcretA.accepter(visiteurConcret1);
        elementConcretB.accepter(visiteurConcret2);
    }
}
