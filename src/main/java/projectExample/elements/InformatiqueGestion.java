package VisitorPattern.projectExample.elements;

import VisitorPattern.projectExample.visitors.HegVisitor;

import java.util.ArrayList;
import java.util.List;

public class InformatiqueGestion extends Heg{

    protected List<String> langagesProgrammation;

    public InformatiqueGestion(int difficulte, String exigence, List<String> langagesProgrammation) {
        super(difficulte, exigence);
        this.langagesProgrammation = langagesProgrammation;
    }

    @Override
    public String accepter(HegVisitor visitor) {
        return visitor.visiterFig(this);
    }

    public List<String> getLangagesProgrammation() {
        return langagesProgrammation;
    }

    public void setLangagesProgrammation(List<String> langagesProgrammation) {
        this.langagesProgrammation = langagesProgrammation;
    }
}
