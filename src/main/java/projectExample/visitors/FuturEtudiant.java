package VisitorPattern.projectExample.visitors;

import VisitorPattern.projectExample.elements.EconomieEntreprise;
import VisitorPattern.projectExample.elements.InformatiqueGestion;
import VisitorPattern.projectExample.elements.Tourisme;

public class FuturEtudiant implements HegVisitor{
    @Override
    public String visiterFig(InformatiqueGestion fig) {
        return "Pour intégrer la filière informatique de gestion, il faut " + fig.getExigence();
    }

    @Override
    public String visiterFee(EconomieEntreprise fee) {
        return "Pour intégrer la filière économie d'entreprise, il faut " + fee.getExigence();
    }

    @Override
    public String visiterFto(Tourisme fto) {
        return "Pour intégrer la filière tourisme, il faut " + fto.getExigence();
    }
}
