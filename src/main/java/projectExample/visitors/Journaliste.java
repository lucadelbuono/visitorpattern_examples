package VisitorPattern.projectExample.visitors;

import VisitorPattern.projectExample.elements.EconomieEntreprise;
import VisitorPattern.projectExample.elements.InformatiqueGestion;
import VisitorPattern.projectExample.elements.Tourisme;

public class Journaliste implements HegVisitor{
    @Override
    public String visiterFig(InformatiqueGestion fig) {
        return "la filière informatique de gestion a une difficulté de " + fig.getDifficulte() + " sur 10.";
    }

    @Override
    public String visiterFee(EconomieEntreprise fee) {
        return "la filière économie d'entreprise a une difficulté de " + fee.getDifficulte() + " sur 10.";
    }

    @Override
    public String visiterFto(Tourisme fto) {
        return "la filière tourisme a une difficulté de " + fto.getDifficulte() + " sur 10.";
    }
}
