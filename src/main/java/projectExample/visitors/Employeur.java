package VisitorPattern.projectExample.visitors;

import VisitorPattern.projectExample.elements.EconomieEntreprise;
import VisitorPattern.projectExample.elements.InformatiqueGestion;
import VisitorPattern.projectExample.elements.Tourisme;

public class Employeur implements HegVisitor{
    @Override
    public String visiterFig(InformatiqueGestion fig) {
        return "En informatique, les langages suivants: " + fig.getLangagesProgrammation() + " sont pratiqués.";
    }

    @Override
    public String visiterFee(EconomieEntreprise fee) {
        return "En économie, les options suivantes: " + fee.getOptions() + " sont disponibles.";
    }

    @Override
    public String visiterFto(Tourisme fto) {
        return "En tourisme, les langues suivantes: " + fto.getLangues() + " sont apprises.";
    }
}
