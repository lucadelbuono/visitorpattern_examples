# Patron de conception Visiteur
**Auteurs:** François Brouchoud & Luca Del Buono

Ce projet a pour but de fournir deux examples du patron de conception Visiteur. 

### Premier exemple
Le premier exemple est un exemple générique tiré du livre "Design Patterns: Elements of Reusable Object-Oriented Software". 

### Deuxième exemple
Le deuxième exemple est un exemple inventé dans le cadre du cours "634-1: Design Patterns" de la filière informatique de gestion de la HES-SO Valais-Wallis. 
